"use strict";
/*
1. Функція – це окремий шматок коду, підпрограма, яка вбудовується у програму. Функції потрібні для полегшення роботи, спрощення та скорочення коду.
2. Щоб під час виклику функції вона виконувала свою роботу, до неї потрібно вставити аргументи, з якими ця функція виконуватиме обчислення, дії.
3. Оператор return завершує виконання поточної функції та повертає її значення. При виклику оператора return у функції її виконання припиняється.
*/
let firstNum = "";
let secondNum = "";
let mathSymb = "";
while (!Number.isFinite(firstNum) || firstNum === 0 || !Number.isFinite(secondNum) || secondNum === 0) {
    firstNum = +prompt("Please enter the first number: ", "");
    secondNum = +prompt("Please enter the second number: ", "");
    if (!Number.isFinite(firstNum) || firstNum === 0 || !Number.isFinite(secondNum) || secondNum === 0) {
        alert("Error! Please be more attentive this time while entering numbers.");
    }
}
while (mathSymb !== "+" && mathSymb !== "-" && mathSymb !== "/" && mathSymb !== "*") {
    mathSymb = prompt("Enter the symbol of the math operation '+' or '-' or '/' or '*'");
}
function calculateTwoNum (a, b, symb) {
    let result
    switch (symb) {
        case "+":
            result = a + b;
            break;
        case "-":
            result = a - b;
            break;
        case "/":
            result = a / b;
            break;
        case "*":
            result = a * b;
            break;
    }
    return result;
}
const res = calculateTwoNum(firstNum, secondNum, mathSymb);
console.log(`${firstNum} ${mathSymb} ${secondNum} = ${res}`)